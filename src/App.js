import { useState } from "react";
import styled, { createGlobalStyle } from "styled-components";

import Login from "./pages/Login";
import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import Treinos from "./pages/Treinos";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Dashboard from "./pages/Dashboard";
import TreinoDetalhes from "./pages/TreinoDetalhes";
import { ToastContainer } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";

const GlobalStyles = createGlobalStyle`
  *{
    margin: 0;
    padding: 0;
    outline: none;
    box-sizing: border-box;
    text-decoration: none;
    font-family: Roboto, sans-serif;
    list-style: none;
  }
  body {
    background-color: #cecae8;
  }
`;

const App = () => {
  const [isAuthenticated, setIsAuthenticated] = useState(
    JSON.parse(localStorage.getItem("pro-fit"))
  );

  return (
    <Main>
      <GlobalStyles />
      <ToastContainer
        position="top-right"
        autoClose={2000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
      <Header
        isAuthenticated={isAuthenticated}
        setIsAuthenticated={setIsAuthenticated}
      />

      <Routes>
        <Route path="/" element={<Home />} />
        <Route
          path="/login"
          element={
            <Login
              setIsAuthenticated={setIsAuthenticated}
              isAuthenticated={isAuthenticated}
            />
          }
        />
        <Route
          path="/treinos"
          element={<Treinos isAuthenticated={isAuthenticated} />}
        />
        <Route
          path="/treinos/:id"
          element={<TreinoDetalhes isAuthenticated={isAuthenticated} />}
        />
        <Route path="/dashboard" element={<Dashboard />} />
      </Routes>
      <Footer />
    </Main>
  );
};

const Main = styled.main`
  padding-top: 75px;
`;

export default App;
