import { useState } from "react";
import { useNavigate } from "react-router-dom";
import api from "../../services/api";
import { toast } from "react-toastify";
import styled from "styled-components";

import Input from "../Input";
import Button from "../Button";

const RegisterForm = ({ setVisualizacao }) => {
  const navigate = useNavigate();
  const [nome, setNome] = useState("");
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");
  const [funcao, setFuncao] = useState("");
  const handleSubmit = async () => {
    try {
      await api.post("usuario", {
        nome: nome,
        email: email,
        senha: senha,
        funcao: funcao,
      });
      toast.success("Usuário Cadastrado!");
      navigate("/treinos");
    } catch {
      toast.error("Não foi possível cadastrar usuário");
    }
  };

  return (
    <Container>
      <Bemvindo>Vamos começar!</Bemvindo>
      <Form>
        <Input
          placeholder="Nome"
          type="nome"
          value={nome}
          onChange={(e) => setNome(e.target.value)}
        />
        <Input
          placeholder="Email"
          type="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Input
          placeholder="Senha"
          type="password"
          value={senha}
          onChange={(e) => setSenha(e.target.value)}
        />
        <Input
          placeholder="Função"
          type="funcao"
          value={funcao}
          onChange={(e) => setFuncao(e.target.value)}
        />
        <Row>
          <Button type="button" onClick={() => handleSubmit()}>
            Entrar
          </Button>
          <Button
            style={{ marginLeft: "4px" }}
            type="button"
            onClick={() => setVisualizacao("loginForm")}
          >
            Voltar
          </Button>
        </Row>
      </Form>
    </Container>
  );
};

const Row = styled.div`
  display: flex;
`;

const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  min-height: 650px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

const Bemvindo = styled.span`
  text-align: center;
  font-size: 18px;
  text-transform: uppercase;
  margin-top: 0px;
  margin-bottom: 20px;
  color: purple;
`;

export default RegisterForm;
