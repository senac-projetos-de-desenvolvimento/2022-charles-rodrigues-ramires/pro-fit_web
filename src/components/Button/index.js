import { forwardRef } from "react";
import styled from "styled-components";

const Button = forwardRef(({ children, ...rest }, ref) => (
  <StyledButton ref={ref} {...rest}>
    {children}
  </StyledButton>
));

const StyledButton = styled.button`
  width: 100%;
  height: 40px;
  box-sizing: border-box;
  border-radius: 5px;
  border: 1px solid #e6e6e6;
  cursor: pointer;
  font-size: 14px;
  font-weight: bold;
  text-transform: uppercase;
  outline: none;
  margin-bottom: 10px;
  padding: 10px;
  background-color: purple;
  color: white;
  &:hover {
    background-color: rgb(186, 85, 211);
  }
`;

export default Button;
