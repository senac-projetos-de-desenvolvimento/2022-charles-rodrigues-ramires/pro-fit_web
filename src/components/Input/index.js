import { forwardRef } from "react";
import styled from "styled-components";

const Input = forwardRef((props, ref) => <StyledInput ref={ref} {...props} />);

const StyledInput = styled.input`
  width: 100%;
  height: 42px;
  box-sizing: border-box;
  border-radius: 5px;
  border: 1px solid #e6e6e6;
  margin-bottom: 20px;
  font-size: 15px;
  padding: 10px;
  outline: none;
`;

export default Input;
