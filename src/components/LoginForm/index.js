import { useState } from "react";
import { useNavigate } from "react-router-dom";
import api from "../../services/api";
import { toast } from "react-toastify";
import styled from "styled-components";
import Input from "../Input";
import Button from "../Button";

const LoginForm = ({ setIsAuthenticated, setVisualizacao }) => {
  const navigate = useNavigate();
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");

  const handleSubmit = async () => {
    try {
      const res = await api.post("usuario/login", {
        email: email,
        senha: senha,
      });
      console.log(res);
      setIsAuthenticated(res.data.acessToken);
      localStorage.setItem("pro-fit", JSON.stringify(res.data.acessToken));
      localStorage.setItem("user", JSON.stringify(res.data));
      toast.success("Logado com sucesso");
      navigate("/treinos");
    } catch {
      toast.error("Email ou senha incorretos.");
    }
  };

  return (
    <Container>
      <Bemvindo>Bem Vindo!</Bemvindo>
      <Login>
        <Input
          placeholder="Email"
          className={email !== "" ? "has-val input" : "input"}
          type="email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <Input
          placeholder="Senha"
          className={senha !== "" ? "has-val input" : "input"}
          type="password"
          value={senha}
          onChange={(e) => setSenha(e.target.value)}
        />{" "}
        <Button type="button" onClick={() => handleSubmit()}>
          Entrar
        </Button>
        <Button type="button" onClick={() => setVisualizacao("Cadastrar")}>
          Cadastre-se
        </Button>
      </Login>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  width: 100%;
  height: 100%;
  min-height: 650px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Bemvindo = styled.span`
  text-align: center;
  font-size: 18px;
  text-transform: uppercase;
  margin-top: 0px;
  margin-bottom: 20px;
  color: purple;
`;

const Login = styled.div`
  width: 320px;
`;

export default LoginForm;
