import styled from "styled-components";

const Section = ({ title = "", content = "", ...rest }) => {
  return (
    <Container {...rest}>
      <Title>{title}</Title>
      <Content>{content}</Content>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  max-width: 1280px;
  padding: 20px;
  margin: 10px 0;
`;
const Title = styled.h1`
  font-size: 36px;
  margin: 15px 0;
  color: #483d8b;
`;
const Content = styled.p`
  font-size: 24px;
  color: #090f57;
  line-height: 32px;
  max-width: 820px;
`;

export default Section;
