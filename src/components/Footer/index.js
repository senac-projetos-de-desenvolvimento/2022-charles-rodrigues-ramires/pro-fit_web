import styled from "styled-components";

const Footer = () => {
  return (
    <Container>
      <List>
        <Item>
          <p>Nome: Charles Ramires</p>
        </Item>
        <Item>
          <p>Whatsapp: (53)999999999</p>
        </Item>
        <Item>
          <p>E-mail: crramires@gmail.com</p>
        </Item>
      </List>
    </Container>
  );
};

const Container = styled.div`
  width: 100%;
  display: flex;
  position: fixed;
  bottom: 0;
  justify-content: center;
  padding: 10px 10px;
  box-shadow: -1px 1px 23px 0px rgba(0, 0, 0, 0.75);
  -webkit-box-shadow: -1px 1px 23px 0px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: -1px 1px 23px 0px rgba(0, 0, 0, 0.75);
  background: rgb(9, 15, 87);
  background: linear-gradient(
    185deg,
    rgba(9, 15, 87, 1) 0%,
    rgba(49, 44, 120, 1) 39%,
    rgba(72, 61, 139, 1) 86%
  );
`;

const List = styled.ul``;

const Item = styled.li`
  font-size: 18px;
  color: #fff;
  font-weight: bold;
  margin: 15px 0;
  text-align: center;
`;

export default Footer;
