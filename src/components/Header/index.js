import styled from "styled-components";
import { Link } from "react-router-dom";

const Header = ({ setIsAuthenticated, isAuthenticated }) => {
  const user = JSON.parse(localStorage.getItem("user"));

  return (
    <Navbar>
      <StyledLink to="/" fontSize="28px" fontWeight="bold">
        Home
      </StyledLink>
      <Navigation>
        <StyledLink to="/treinos" fontSize="22px" margin="0 10px">
          Treinos
        </StyledLink>

        {user?.funcao === "instrutor" && (
          <StyledLink to="/dashboard" fontSize="24px" margin="0 10px">
            Dashboard
          </StyledLink>
        )}

        {isAuthenticated ? (
          <StyledLink
            to="/"
            fontSize="24px"
            margin="0 10px"
            onClick={() => setIsAuthenticated(false)}
          >
            Logout
          </StyledLink>
        ) : (
          <StyledLink to="/login" fontSize="24px" margin="0 10px">
            Login
          </StyledLink>
        )}
      </Navigation>
    </Navbar>
  );
};

const Navbar = styled.nav`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: fixed;
  top: 0;
  padding: 20px 30px;
  box-shadow: -1px 1px 23px 0px rgba(0, 0, 0, 0.75);
  -webkit-box-shadow: -1px 1px 23px 0px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: -1px 1px 23px 0px rgba(0, 0, 0, 0.75);
  background: rgb(9, 15, 87);
  background: linear-gradient(
    185deg,
    rgba(9, 15, 87, 1) 0%,
    rgba(49, 44, 120, 1) 39%,
    rgba(72, 61, 139, 1) 86%
  );
`;

const StyledLink = styled(Link)`
  font-size: ${({ fontSize = "12px" }) => fontSize};
  font-weight: ${({ fontWeight = "normal" }) => fontWeight};
  color: ${({ color = "#fff" }) => color};
  margin: ${({ margin = "0" }) => margin};
  transition: all 0.5s ease;
  border-radius: 5px;
  &:hover {
    background: #483d8b;
  }
`;

const Navigation = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;
`;

export default Header;
