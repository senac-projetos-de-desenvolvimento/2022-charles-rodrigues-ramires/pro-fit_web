import { useEffect, useState } from "react";
import styled from "styled-components";
import api from "../../services/api";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import { TextField, CircularProgress, Input, Box } from "@mui/material";
import {
  Dialog,
  DialogActions,
  DialogTitle,
  Autocomplete,
  Typography,
  Rating,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import FitnessCenterIcon from "@mui/icons-material/FitnessCenter";

const Treinos = ({ isAuthenticated }) => {
  const [treinos, setTreinos] = useState([]);
  const [filtroTreinos, setFiltroTreinos] = useState([]);
  const [pesquisa, setPesquisa] = useState("");
  const [open, setOpen] = useState(false);
  const [loading, setLoading] = useState(false);
  const [inputTreino, setInputTreino] = useState({
    nomeTreino: "",
    dificuldade: "",
    listExercicios: [],
  });

  const user = JSON.parse(localStorage.getItem("user"));

  const listaDificuldade = [
    { label: "iniciante", value: "iniciante" },
    { label: "intermediario", value: "intermediario" },
    { label: "avançado", value: "avançado" },
  ];

  const [exercicios, setExercicios] = useState({ nomeExercicio: "" });

  const navigate = useNavigate();

  const handleGet = async () => {
    try {
      setLoading(true);
      const { data } = await api.get(`treinos`, { withCredentials: false });
      setTreinos(data);
      setFiltroTreinos(data);
    } catch {
      toast.error("Não foi possível listar treinos.");
    } finally {
      setLoading(false);
    }
  };

  const handleGetExercicios = async () => {
    try {
      const { data } = await api.get(`exercicios`, exercicios);
      setExercicios(data);
    } catch {
      toast.error("Não foi possível listar exercicios");
    }
  };

  const handlePesquisa = (e) => {
    setPesquisa(e.target.value);
    const filtroData = e.target.value
      ? treinos.filter((item) =>
          item.dificuldade.toLowerCase().includes(e.target.value.toLowerCase())
        )
      : treinos;
    setFiltroTreinos(filtroData);
  };

  const handleSubmit = async () => {
    try {
      inputTreino.listExercicios = inputTreino.exercicios.map(
        (item) => item.id
      );

      const { data } = await api.post(`treinos`, inputTreino);
      setInputTreino({
        nomeTreino: "",
        dificuldade: "",
        listExercicios: [],
      });
      setOpen(false);
      handleGet();
    } catch {
      toast.error("Não foi possivel criar treino.");
    }
  };

  useEffect(() => {
    handleGet();
    handleGetExercicios();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleNavigation = (treinoId) => {
    isAuthenticated
      ? navigate(`/treinos/${treinoId}`)
      : toast.error("Você precisa estar logado para ver os exercícios.");
  };

  return (
    <Box margin="20px">
      {user?.funcao === "instrutor" && (
        <AddSaveTreino
          type="button"
          className="buttonAddTreino"
          onClick={() => setOpen(true)}
        >
          Adicionar Treino
        </AddSaveTreino>
      )}
      <ContainerPesquisa>
        <h1>Treinos Disponíveis:</h1>
        <Pesquisa
          className="input-pesquisa"
          type="text"
          value={pesquisa}
          onChange={(e) => handlePesquisa(e)}
          placeholder="Pesquise pela dificuldade do treino..."
        />
      </ContainerPesquisa>
      <ContainerList>
        {filtroTreinos.length > 0 ? (
          filtroTreinos.map((treino, index) => (
            <CardTreinos
              delay={`${index / 10}s`}
              onClick={() => handleNavigation(treino.id)}
            >
              <Typography>
                <Typography component="span" fontWeight="bold">
                  Nome:
                </Typography>
                {treino.nomeTreino}
              </Typography>

              <Typography>
                <Typography component="span" fontWeight="bold">
                  Dificuldade:
                </Typography>
                {treino.dificuldade}
              </Typography>
              <Typography>
                <Typography>
                  <Typography component="span" fontWeight="bold">
                    Avaliação:
                  </Typography>
                </Typography>
                <Rating
                  name="read-only"
                  value={treino.avaliacao}
                  readOnly
                  icon={
                    <FitnessCenterIcon fontSize="inherit" color="secondary" />
                  }
                  emptyIcon={<FitnessCenterIcon fontSize="inherit" />}
                />
              </Typography>
            </CardTreinos>
          ))
        ) : loading ? (
          <ContainerLoader>
            <CircularProgress />
          </ContainerLoader>
        ) : (
          <TextoVazio>Nenhum treino disponível</TextoVazio>
        )}
      </ContainerList>
      <Dialog
        fullWidth
        maxWidth={"md"}
        open={open}
        onClose={() => setOpen(false)}
      >
        <DialogTitle display={"flex"} justifyContent={"space-between"}>
          <div>Adicionando Treino</div>
          <CloseIcon
            style={{ cursor: "pointer" }}
            onClick={() => setOpen(false)}
          />
        </DialogTitle>
        <ModalContainer>
          <Input
            sx={{ display: "block", marginBottom: "10px" }}
            type="text"
            placeholder="Nome do treino"
            value={inputTreino.nomeTreino}
            onChange={(e) =>
              setInputTreino({ ...inputTreino, nomeTreino: e.target.value })
            }
          />

          <Autocomplete
            sx={{ marginTop: "10px" }}
            name="dificuldade"
            options={listaDificuldade}
            getOptionLabel={(option) => option.label}
            onChange={(event, option) => {
              setInputTreino({ ...inputTreino, dificuldade: option.value });
            }}
            inputValue={inputTreino.exercicios}
            onInputChange={(event, option) => {
              setInputTreino({ ...inputTreino, dificuldade: option.value });
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Dificuldade"
                placeholder="Selecione a dificuldade"
              />
            )}
          />

          <Autocomplete
            sx={{ marginTop: "10px" }}
            multiple
            name="exercicios"
            options={exercicios}
            getOptionLabel={(option) => option.nomeExercicio}
            onChange={(event, option) => {
              setInputTreino({ ...inputTreino, exercicios: option });
            }}
            inputValue={inputTreino.exercicios}
            onInputChange={(event, option) => {
              setInputTreino({ ...inputTreino, exercicios: option });
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                variant="standard"
                label="Exercicios"
                placeholder="Selecione os exercícios"
              />
            )}
          />
          <DialogActions>
            <AddSaveTreino
              type="button"
              className="buttonAddTreino"
              onClick={() => handleSubmit()}
            >
              Salvar Treino
            </AddSaveTreino>
          </DialogActions>
        </ModalContainer>
      </Dialog>
    </Box>
  );
};

const ContainerLoader = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 450px;
`;

const ContainerList = styled.div`
  display: flex;
  flex-wrap: wrap;
  gap: 5px 10px;
`;

const CardTreinos = styled.div`
  max-width: 280px;
  width: 100%;
  border-radius: 10px;
  padding: 30px;
  border: 1px solid #e5e5e5;
  opacity: 0;
  animation: fadein 1s forwards ease;
  animation-delay: ${({ delay }) => delay};
  transition: all 0.5s ease;
  cursor: pointer;
  &:hover {
    background-color: #fff;
  }
  @keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`;

const AddSaveTreino = styled.button`
  padding: 5px 10px;
  border-radius: 10px;
  border: none;
  background-color: purple;
  color: white;
  cursor: pointer;
  transition: all 0.3s ease;
  margin: 10px 0 10px 10px;
  &:hover {
    background-color: rgb(186, 85, 211);
  }
`;

const ContainerPesquisa = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin: 20px;
`;

const Pesquisa = styled.input`
  width: 100%;
  max-width: 300px;
  border-radius: 4px;
  padding: 3px 10px;
  outline: none;
`;

const TextoVazio = styled.p`
  margin: 20px 5px;
`;

const ModalContainer = styled.div`
  width: 100%;
  padding: 20px;
  display: block;
  margin-top: 10px;
`;

export default Treinos;
