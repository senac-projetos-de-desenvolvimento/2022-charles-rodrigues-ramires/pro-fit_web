import styled from "styled-components";
import Section from "../../components/Section";

const Home = () => {
  return (
    <Container>
      <Section
        title="Quem somos?"
        content="Somos uma empresa que visa disponibilizar às pessoas treinos
              estruturados por profissionais capacitados na modalidade online.
              Assim, em nossa plataforma, tanto pessoas que buscam auxílio para
              realização de atividades física como profissionais que desejam
              trabalhar nessa modalidade, serão bem-vindos."
      />
      <Section
        title="Nossa motivação"
        content="  É comprovado que atividade física tem diversos benefícios para saúde e
          entre essas atividades está a musculação e seus benefícios.Pensando
          nisso veio a ideia de criar essa aplicação para auxiliar quem está
          começando a prática de exercícios através da musculação e a ajudar
          quem tem o conhecimento na área a divulgar seu trabalho.
        "
      />
    </Container>
  );
};

const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export default Home;
