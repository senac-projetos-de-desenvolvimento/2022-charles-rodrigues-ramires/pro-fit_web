import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import styled from "styled-components";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  LineController,
  BarController,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { Box, CircularProgress } from "@mui/material";

import api from "../../services/api";

ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  PointElement,
  LineElement,
  LineController,
  BarController,
  Title,
  Tooltip,
  Legend
);

const labels = ["1", "2", "3", "4", "5"];

export const options = {
  resposible: true,
  plugins: {
    legend: {
      position: "top",
    },
    title: {
      display: true,
      text: "Avaliação de treino",
    },
  },
};

const Dashboard = () => {
  const [treinos, setTreinos] = useState([]);
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const user = JSON.parse(localStorage.getItem("user"));

  const handleGet = async () => {
    try {
      setLoading(true);
      const { data } = await api.get(`treinos`, { withCredentials: false });
      const formatData = data.reduce(
        (acc, cur) => ({ ...acc, [cur.avaliacao]: acc[cur.avaliacao] + 1 }),
        {
          1: 0,
          2: 0,
          3: 0,
          4: 0,
          5: 0,
        }
      );
      setTreinos(formatData);
    } catch {
      toast.error("Não foi possível listar treinos.");
    } finally {
      setLoading(false);
    }
  };

  const data = {
    labels,
    datasets: [
      {
        label: "Avaliação",
        data: treinos,
        borderColor: "rgb(255, 99, 132)",
        backgroundColor: "rgba(255, 99, 132, 0.5)",
      },
    ],
  };

  useEffect(() => {
    if (user?.funcao !== "instrutor") {
      navigate("/");
    }
    handleGet();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Box width="100%" margin="20px 0" display="flex" justifyContent="center">
      <Container>
        {loading ? (
          <Box>
            <CircularProgress />
          </Box>
        ) : (
          <Bar data={data} options={options} />
        )}
      </Container>
    </Box>
  );
};

const Container = styled.div`
  width: 100%;
  max-width: 850px;
  height: 600px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export default Dashboard;
