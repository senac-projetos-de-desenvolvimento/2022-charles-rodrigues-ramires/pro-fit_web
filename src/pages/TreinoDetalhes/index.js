import { useEffect, useState } from "react";
import api from "../../services/api";
import { useParams, useNavigate } from "react-router-dom";
import { DataGrid, ptBR } from "@mui/x-data-grid";
import { toast } from "react-toastify";
import { Typography, Rating, CircularProgress } from "@mui/material";
import FitnessCenterIcon from "@mui/icons-material/FitnessCenter";
import Button from "../../components/Button";

const columns = [
  { field: "nomeExercicio", headerName: "Exercicio", width: 270 },
  { field: "serie", headerName: "Series", width: 130 },
  { field: "descanso", headerName: "Descanso", width: 130 },
  { field: "repeticao", headerName: "Repetições", width: 130 },
  {
    field: "foto",
    headerName: "Imagem",
    width: 130,
    flex: 1,
    renderCell: (row) => (
      <a href={row.value} target="_blank" rel="noreferrer">
        Clique para ver exercício
      </a>
    ),
  },
];

const TreinoDetalhes = ({ isAuthenticated }) => {
  const [exercicios, setExercicios] = useState([]);
  const { id } = useParams();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const [value, setValue] = useState(null);

  const user = JSON.parse(localStorage.getItem("user"));

  const handleGet = async () => {
    setLoading(true);
    try {
      const data = await api.get(`treino_has_exercicio/${id}`, {
        headers: {
          Authorization: `Bearer ${JSON.parse(
            localStorage.getItem("pro-fit")
          )}`,
        },
      });

      const format = data.data.map((item) => ({ ...item.exercicio }));
      setExercicios(format);
    } catch (error) {
      toast.error("Não foi possível listar treinos.");
    } finally {
      setLoading(false);
    }
  };

  const handleSubmitAvaliacao = async () => {
    try {
      await api.patch(`treinos/avaliacao/${id}`, { avaliacao: value });
      navigate("/treinos");
    } catch {
      toast.error("Não foi possível avaliar esse treino.");
    }
  };

  const handleChange = async (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    if (!isAuthenticated) {
      toast.error("Você precisa estar logado para ver os exercícios.");
      return navigate("/treinos");
    } else {
      handleGet();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      {loading ? (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "100%",
            height: "500px",
          }}
        >
          <CircularProgress />
        </div>
      ) : (
        <div
          style={{
            maxWidth: "1280px",
            margin: "40px auto 0",
            width: "100%",
          }}
        >
          <DataGrid
            rows={exercicios}
            columns={columns}
            pageSize={99}
            autoHeight
            rowsPerPageOptions={[5]}
            localeText={{
              ...ptBR.components.MuiDataGrid.defaultProps.localeText,
              toolbarColumns: "selecionar colunas",
              noRowsLabel: "Nenhum registro encontrado",
            }}
          />
          {user?.funcao === "aluno" && (
            <div style={{ margin: "20px 0" }}>
              <Typography component="legend">Avalie esse treino:</Typography>
              <Rating
                name="simple-controlled"
                value={value}
                onChange={handleChange}
                icon={
                  <FitnessCenterIcon fontSize="inherit" color="secondary" />
                }
                emptyIcon={<FitnessCenterIcon fontSize="inherit" />}
              />
              <Button type="button" onClick={() => handleSubmitAvaliacao()}>
                Enviar Avaliação.
              </Button>
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default TreinoDetalhes;
